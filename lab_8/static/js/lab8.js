  // FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '152562575371309',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });
        FB.getLoginStatus((response) => {
      const loggedIn = response.status === 'connected';

      render(loggedIn);
    });


    // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
    // dan jalankanlah fungsi render di bawah, dengan parameter true jika
    // status login terkoneksi (connected)

    // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
    // otomatis akan ditampilkan view sudah login
  };

  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));




  // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
  const render = loginFlag => {
    if (loginFlag) {
      console.log ("masuk ke render");
      // Jika yang akan dirender adalah tampilan sudah login

      // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
      getUserData(user => {
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        $('#lab8').html(
          '<div class="profile" >' +
            '<img align width="150" height="50" class="cover" src="' + user.cover.source + '" alt="cover" />' +
            '<p></p> '+
            '<img width="150" height="150" class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
            '<div class="data">' +
              '<h1>' + user.name + '</h1>' + 
              '<h2>'  + user.gender + '</h2>' +
            '</div>' +
          '</div>' +

          '<button style = " size : 40px" class="logout" onclick="facebookLogout()">Logout</button>'+ '<p></p>' +
          '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' + 
          '<button class="postStatus"  onclick="jembatanPesan()">Post ke Facebook</button>'
        );

        // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        getUserFeed(feed => {
          feed.data.map(value => {
            console.log("sudah dimethod feed")
            // Render feed, kustomisasi sesuai kebutuhan.
            if (value.message && value.story) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h3>' + value.message + '</h3>' +
                  '<button class="hapus"  ">hapus</button>'+
                  '<h3>' + value.story + '</h3>' +
                  '<button class="hapus" ">hapus</button>'+
                '</div>'
              );
            } else if (value.message) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h3>' + value.message + '</h3>' +
                  '<button class="hapus"  ">hapus</button>'+
                '</div>'
              );
            } else if (value.story) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h3>' + value.story + '</h3>' +
                  '<button class="hapus"  ">hapus</button>'+
                '</div>'
              );
            }
          });
        });
      });

    } else {
      // Tampilan ketika belum login
      $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
    }
  };


  function jembatanPesan(){
    console.log("jemabatan pesan");
    var pesan = document.getElementById('postInput').value;

    postStatus(pesan);
   
  };

  // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
    // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
    // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
  const facebookLogin = () => {
    console.log ("masuk login");
    //function facebookLogin(){
       FB.login(function(response){
        if (response.status === 'connected'){
          loginFlag = true;
          console.log(response);
        }
        else {
          loginFlag = false;
        }
         console.log(response);
         render (loginFlag);
       }, {scope:'public_profile, user_posts, publish_actions, user_about_me, email'}) 
  };

    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
  //};
  const facebookLogout = () => {
    console.log("masuk logout");
    //function facebookLogout(){
     FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
         // console.log("hahha")
          FB.logout();
          render(false);
        }
     });
 }

  // TODO: Lengkapi Method Ini
  // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
  // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
  // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
  // meneruskan response yang didapat ke fungsi callback tersebut
  // Apakah yang dimaksud dengan fungsi callback?
  const getUserData = (fun) => {
   FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.api('/me?fields=id,name, gender, email, cover, about, picture' , 'GET', function(response){
            fun (response); //TANYA INI
            console.log("dibawah ini data yang diambil")
            console.log(response);
          });
        }
    });
}

  const getUserFeed = (fun) => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        FB.api('/me/posts', 'GET', function(response){
          console.log(response);
          fun(response);
          console.log(response);
        });
      }
  });
};

  const postFeed = (pesan) => {
    // Todo: Implement method ini,
    // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
    // Melalui API Facebook dengan message yang diterima dari parameter.
    var message = pesan;

    FB.api('/me/feed', 'POST', {message:message});

  };

  const postStatus = () => {
    console.log("ini post status");
    const message = $('#postInput').val();
    postFeed(message);
  };
