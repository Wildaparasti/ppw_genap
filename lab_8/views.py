from django.shortcuts import render


def index(request):
    response = {}
    return render(request, 'lab_8/lab_8.html', response)
