var awal = "msg-send";
function enterFunc(e) {
    var x = document.getElementById("enter-msg").value;
    
    if(e.keyCode == 13) {
        if (x!="") {
            var node = document.createElement("DIV");
            if (awal == "msg-send") {
                awal = "msg-receive";
            }
            else {
                awal = "msg-send";
            }
            node.className = awal;
            var textnode = document.createTextNode(x);
            node.appendChild(textnode);
            document.getElementById("result-msg").appendChild(node);
            document.getElementById("enter-msg").value = "";
            e.preventDefault();
        }
    }

}
