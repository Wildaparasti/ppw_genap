# Lab 1: Introduction to Git (on GitLab) & TDD (Test-Driven Development) with Django

CSGE602022 - Perancangan & Pemrograman Web @
Fakultas Ilmu Komputer - Universitas Indonesia, Semester Genap 2017/2018

* * *

## Tujuan Pembelajaran

Setelah menyelesaikan latihan ini, siswa akan dapat:

- Mengenal & menggunakan perintah Git 
- Konfigurasi repositori lokal dan online (GitLab)
- Pengembangan TDD dasar
- Mempublikasikan karyanya di penyedia layanan awan PaaS (Platform-as-a-Service)

## Kebutuhan praktikum
- Text Editor (apapun merk-nya, yang penting bisa menulis & edit text)
- Python 3.6 terinstall di komputer Anda
- Akses internet
- Git client

## Instruksi Lab 1: Pengenalan Test-Driven Development (TDD) menggunakan Django

1. Mendownload kode contoh dari Server kode PPW-2017-Genap
> - Melakukan GIT Clone https://gitlab.com/PPW-2017/ppw-lab-genap.git ke komputer lokal. 
> - Lakukan dengan cara: git clone https://gitlab.com/PPW-2017/ppw-lab-genap.git [direktori tujuan clone lokal Anda]
> - cth: git clone https://gitlab.com/PPW-2017/ppw-lab-genap.git **sumber-kode-instruksi**

2. Membuat project Anda sendiri pada GitLab
> - Buatlah login pada server gitlab.com
> - Buatlah sebuah project
> - Lakukan GIT Clone dari project Anda ke direktori tujuan clone lokal Anda (cth: dari https://gitlab.com/adin1/coba1.git)
> - cth: git clone https://gitlab.com/adin1/coba1.git **kode-saya**

3. Copy berkas-berkas dan direktori-direktori berikut dari **sumber-kode-instruksi**:
> - Berkas:
>   - .gitlab-ci.yml
>   - Procfile
>   - deployment.sh
>   - manage.py
>   - requirements.txt
> - Direktori **praktikum** lengkap dengan isinya
>
> - Copy seluruh file dan direktori di atas ini ke project lokal Anda (cth: ke direktori **kode-saya**), sehingga direktori Anda akan berisi berkas-berkas & direktori sbb:
>   - .gitlab-ci.yml
>   - Procfile
>   - deployment.sh
>   - manage.py
>   - requirements.txt
>   - praktikum/
>       - __init__.py
>       - settings.py
>       - urls.py
>       - wsgi.py

4. Gunakan shell/command prompt kesukaan Anda dan pastikan Anda sudah berada di dalam direktori **kode-saya**/

5. Membuat sebuah **virtual environment** dengan perintah:

    ```bash
    python -m venv env
    ```
    
    > Pastikan kalau Anda meng-eksekusi command di atas ini pada root path kode Anda (cth: pada direktori **kode-saya**)

6. Aktivasi environment lokal dan install kebutuhan paket-paket python yang dibutuhkan:

    Windows:
    
    ```bash
    env\Scripts\activate.bat
    pip install -r requirements.txt
    ```

    Linux & Mac OS:

    ```bash
    source env/bin/activate
    pip install -r requirements.txt
    ```

7. Buat direktori `lab_1` pada direktori **kode-saya**

8. Buat berkas-berkas `lab_1/views.py` yang berisi:

    ```python
    from django.shortcuts import render
    from datetime import datetime, date
    # Enter your name here
    mhs_name = ''  # TODO Implement this
    curr_year = int(datetime.now().strftime("%Y"))
    birth_date = date()  # TODO Implement this, format (Year, Month, Date)
    # Create your views here.
    def index(request):
        response = {'name': mhs_name, 'age': calculate_age(birth_date.year)}
        return render(request, 'index_lab1.html', response)


    def calculate_age(birth_year):
        return None  # TODO implement this

    ```

9. Isi nama Anda pada variabel `mhs_name`

10. Buat berkas-berkas `lab_1/admin.py` yang berisi:

    ```python
    from django.contrib import admin

    ```

11. Buat berkas-berkas `lab_1/apps.py` yang berisi:

    ```python
    from django.apps import AppConfig

    class Lab1Config(AppConfig):
        name = 'lab_1'
    ```

12. Buat berkas-berkas `lab_1/__init__.py` yang berisi:

    ```python
    ```

13. Buat berkas-berkas `lab_1/models.py` yang berisi:

    ```python
    from django.db import models

    # Create your models here.
    ```

14. Buat berkas-berkas `lab_1/tests.py` yang berisi:

    ```python
    from django.test import TestCase
    from django.test import Client
    from django.urls import resolve
    from .views import index, mhs_name, calculate_age
    from django.http import HttpRequest
    from datetime import date
    import unittest


    # Create your tests here.

    class Lab1UnitTest(TestCase):

        def test_hello_name_is_exist(self):
            response = Client().get('/lab-1/')
            self.assertEqual(response.status_code,200)

        def test_using_index_func(self):
            found = resolve('/lab-1/')
            self.assertEqual(found.func, index)

        def test_name_is_changed(self):
            request = HttpRequest()
            response = index(request)
            html_response = response.content.decode('utf8')
            self.assertIn('<title>' + mhs_name + '</title>', html_response)
            self.assertIn('<h1>Hello my name is ' + mhs_name + '</h1>', html_response)
            self.assertFalse(len(mhs_name) == 0)


        def test_calculate_age_is_correct(self):
            self.assertEqual(0, calculate_age(date.today().year))
            self.assertEqual(17, calculate_age(2000))
            self.assertEqual(27, calculate_age(1990))


        def test_index_contains_age(self):
            request = HttpRequest()
            response = index(request)
            html_response = response.content.decode('utf8')
            self.assertRegex(html_response, r'<article>I am [0-9]\d+ years old</article>')
    ```

15. Buat berkas-berkas `lab_1/urls.py` yang berisi:

    ```python
    from django.conf.urls import url
    from .views import index
    #url for app
    urlpatterns = [
        url(r'^$', index, name='index'),
    ]
    ```

16. Buat berkas-berkas `lab_1/templates/index_lab1.html` (NB: buat direktori templates di dalam direktori lab_1 Anda) yang berisi:

    ```python
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>{{ name }}</title>
    </head>
    <body>
        <h1>Hello my name is {{ name }}</h1>
        <article>I am {{ age }} years old</article>
    </body>
    </html>

    ```

17. Mari kita coba menjalankan web server lokal Anda di komputer lokal. Jalankan dengan mengetikkan:

    ```bash
    python manage.py runserver 8000
    ```

    > Pastikan kalau Anda sudah berada di direktori project Anda dan sudah ada file `manage.py`

18. Akses ke web server lokal Anda dengan browser, dengan menuliskan alamat `http://localhost:8000` pada browser Anda.
19. Apakah nama Anda sudah masuk di browser Anda ?

